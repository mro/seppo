#!/bin/sh

cd "$(dirname "$0")" || exit 1

# readonly acc="--header 'Accept: application/xrd+xml'"
# readonly acc="--header 'Accept: application/jrd+json'"

while read -r wefi typ pro
do
  echo "$typ $wefi"
  local_part="$(echo "$wefi" | cut -d @ -f 2)"
  host="$(echo "$wefi" | cut -d @ -f 3)"

  curl \
    --dump-header h \
    --header "Accept: application/jrd+json" \
    --location \
    --max-time 5.0 \
    --silent \
     "https://$host/.well-known/webfinger?resource=acct:${local_part}@${host}" \
   > /dev/null
  echo "  Webfinger $(grep -Ei "^content-type:" h)"
  curl \
    --dump-header h \
    --header "Accept: application/xrd+xml" \
    --location \
    --max-time 5.0 \
    --silent \
     "https://$host/.well-known/host-meta" \
   > /dev/null
  echo "  host-meta $(grep -Ei "^content-type:" h)"
  curl \
    --dump-header h \
    --header "Accept: application/jrd+json" \
    --location \
    --max-time 5.0 \
    --silent \
     "https://$host/.well-known/nodeinfo" \
   > /dev/null
  echo "  nodeinfo $(grep -Ei "^content-type:" h)"
  curl \
    --dump-header h \
    --header "Accept: application/activity+json" \
    --location \
    --max-time 5.0 \
    --silent \
     "$pro" \
   > /dev/null
  echo "  Profile $(grep -Ei "^content-type:" h)"

  # exit 0
  echo ""
done <<-EOF
@administrator@gnusocial.net         GnuSocial  https://gnusocial.net/index.php/user/1
@alfred@libranet.de                  Friendica  https://libranet.de/profile/alfred
@boyter@honk.boyter.org              Honk       https://honk.boyter.org/u/boyter
@dev@microblog.pub                   Micro.blog https://microblog.pub
@edps@tube.network.europa.eu         Peertube   https://tube.network.europa.eu/accounts/edps
@grunfink@comam.es                   snac       https://comam.es/snac/grunfink
@mike@macgirvin.com                  Hubzilla   https://macgirvin.com/channel/mike
@mro_seppo@mastodon.social           Mastodon   https://mastodon.social/users/mro_seppo
@mro@firefish.social                 Firefish   https://firefish.social/users/9j2ot3jvzun7u3hi
@mro@fosstodon.org                   Mastodon   https://fosstodon.social/users/mro
@mro@lemmy.world                     Lemmy      https://lemmy.world/u/mro
@mro@misskey.social                  Misskey    https://misskey.social/users/9j2hlahlgi
@mro@outerheaven.club                Akkoma     https://outerheaven.club/users/mro
@mro@pixelfed.social                 Pixelfed   https://pixelfed.social/users/mro
@takahe@jointakahe.org               Takahe     https://jointakahe.takahe.social/@takahe@jointakahe.org/
@grindhold@demoday.leadfathom.grindhold.de Flohmarkt https://demoday.leadfathom.grindhold.de/users/grindhold
@2023-08-28@dev.seppo.mro.name       Seppo      https://dev.seppo.mro.name/2023-08-28/activitypub/profile.jlda
EOF
