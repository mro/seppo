Part of a maildir[1]-inpired directory structure.

Received create[2] or update activities with notes from subscribed to actors.

Purged after 90 days.

[1]: https://web.archive.org/web/19971012032244/http://www.qmail.org/qmail-manual-html/man5/maildir.html
[2]: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-create
