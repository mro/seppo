Part of a maildir[1]-inpired directory structure.

Files with ongoing non-atomic, transient write operations.

No persistent files.

[1]: https://web.archive.org/web/19971012032244/http://www.qmail.org/qmail-manual-html/man5/maildir.html
