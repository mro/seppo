Extension to the maildir[1]-inpired directory structure.

Postponed jobs with future due date and retry number < 14.
When due, moved to new/.

[1]: https://web.archive.org/web/19971012032244/http://www.qmail.org/qmail-manual-html/man5/maildir.html
