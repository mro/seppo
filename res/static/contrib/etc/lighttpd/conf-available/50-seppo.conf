
index-file.names = ( "index.html", "index.json" )

### #Seppo! begin
var.seppo_path_0 = "/<url path to, but excluding seppo.cgi>/"         # change as needed, keep leading and trailing slash

# Setup
#
# 1. edit above var.seppo_path_0 = ...,
# 2. put this file into /etc/lighttpd/conf-available/
# 3. $ sudo /usr/sbin/lighty-enable-mod seppo
# 4. $ sudo service lighttpd force-reload
# 5. drop seppo.cgi in filesystem location matching seppo_path_0
# 6. point browser to http://my.example.org/.../seppo.cgi

# If $ /usr/sbin/lighty-enable-mod
# lists simple-vhost enabled, merge this config file into
# server.conf inside your vhost directory.
#
# https://redmine.lighttpd.net/projects/1/wiki/TutorialConfiguration
# https://redmine.lighttpd.net/projects/1/wiki/docs_modsimplevhost

server.modules += ("mod_setenv")
# below is a workaround, if 'config_servers' can't be patched:
# $ fgrep server.breakagelog /etc/lighttpd/config_servers
# echo "  server.breakagelog = \"$base/$VHOST/logs/error.log\""
# server.breakagelog = "/var/www/lighttpd/.../logs/error.log"
url.redirect += ( # add to
    # if seppo.cgi is in a sub-directory, delegate (all, global) webfinger to it,
    # see .well-known/webfinger/.htaccess for details.
    "^/(\.well-known/webfinger)\?(.+?&)?(resource=acct(:|%3A)([^&]+)).*$" => seppo_path_0+"$1/index.jrd?$3",
)
$HTTP["url"] =~ "^"+seppo_path_0 {
  cgi.assign = ( # execute without interpreter
    "apchk.cgi" => "",
    "seppo.cgi" => "",
  )
  # https://stackoverflow.com/a/7580289/349514
  # server.error-handler-404 = seppo_path_0 + "themes/current/404.html"
  server.errorfile-prefix = seppo_path_0 + "themes/current/"

  index-file.names = ( "index.html", "index.xml", "index.json" )
  url.redirect = ( ) # replace in case
  url.redirect += ( # add to
    "/(activitypub)/profile\.(jlda|xml)$" => seppo_path_0 + "$1/actor.$2",
  )

  setenv.add-response-header += (
    # nice
    "X-Powered-By"                 => "https://Seppo.mro.name",
    "X-Robots-Tag"                 => "noai, noimageai, GPC",    # https://lobste.rs/s/1phzpg/block_ai_training_on_web_site
    # recommended
    # http://www.golem.de/news/content-security-policy-schutz-vor-cross-site-scripting-1306-99795.html
    # http://www.w3.org/TR/CSP/#example-policies
    "Content-Security-Policy"      => "base-uri 'none'; form-action 'self'; frame-ancestors 'none'; default-src 'none'; style-src 'self' 'unsafe-inline'; script-src 'self' 'sha256-hGqewLn4csF93PEX/0TCk2jdnAytXBZFxFBzKt7wcgo='; connect-src 'self'; font-src 'self'; img-src data: 'self' http://* https://*; media-src 'self';",
    "Referrer-Policy"              => "no-referrer",
    "X-Frame-Options"              => "DENY",
    "X-Content-Type-Options"       => "nosniff",
    # https://lobste.rs/s/98rp8f/cors_is_stupid
    "Access-Control-Allow-Origin"  => "*",
    "Access-Control-Allow-Methods" => "*",
    # check results https://observatory.mozilla.org/analyze/l.mro.name
  )

  mimetype.assign = (
    ".css"   => "text/css; charset=utf-8",
    ".html"  => "text/html; charset=utf-8",
    ".jpg"   => "image/jpeg",
    ".jrd"   => "application/jrd+json",
    ".jsa"   => "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"",
    ".json"  => "application/json",
    ".js"    => "text/javascript; charset=utf-8",
    ".png"   => "image/png",
    ".svg"   => "image/svg+xml",
    ".txt"   => "text/plain; charset=utf-8",
    ".woff2" => "application/font-woff",
    ".woff"  => "application/font-woff",
    ".xml"   => "text/xml; charset=utf-8",
    ".xsl"   => "text/xsl", # a Chromism. https://stackoverflow.com/a/21604288
  )

  deflate.mimetypes = (
    "application/activity+json",
    "application/atom+xml",
    "application/json",
    "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"",
    "application/lrd+json",
    "application/xslt+xml",
    "image/svg+xml",
    "text/",
  )
  deflate.allowed-encodings = ( "zstd", "br", "bzip2", "gzip", "deflate" ) # "bzip2" and "zstd" also supported

  $HTTP["url"] =~ "^"+seppo_path_0+"themes/" { setenv.add-response-header += ( "Cache-Control" => "max-age=604800, public" ) } # 7 days
  else $HTTP["url"] =~ "^"+seppo_path_0+"seppo.cgi/actor/icon" { setenv.add-response-header += ( "Cache-Control" => "max-age=86400, public" ) } # 1 day
  else { setenv.add-response-header += ( "Cache-Control" => "no-cache" ) }
}
$HTTP["url"] =~ "^"+seppo_path_0 + "app/" { url.access-deny = ("") }
#### #Seppo! end

  
$HTTP["url"] !~ "^/\.well-known/" {
  # handle http -> https redirects
  $HTTP["scheme"] == "http" {
    # capture vhost name with regex conditiona -> %0 in redirect pattern
    # must be the most inner block to the redirect rule
#    $HTTP["host"] =~ ".*" { url.redirect = (".*" => "https://%0$0") }
  }
}
