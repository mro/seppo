#!/usr/bin/env dash
set -e
set -u
cd "$(dirname "$0")"

rsync -avPz "$1" "archive.seppo.mro.name:~/mro.name/seppo.archive/tmp/"
ssh archive.seppo.mro.name "sh mro.name/archive.seppo/accept.sh ~/mro.name/seppo.archive/tmp/$(basename "$1")"
