
Q: what diagnose and health tools exist?

A:

$ echo 'https://example.com/foo/o/p-0/#1' \
| ./seppo.cgi abs2rel \
| ./seppo.cgi id2ix \
| ./seppo.cgi ix2s \
| ./seppo.cgi s2atom

$ cat app/var/db/o/p/1.ix \
| ./seppo.cgi ix2s \
| ./seppo.cgi s2atom

$ cat app/var/db/o/p.s \
| ./seppo.cgi s2atom

$ cat <<EOF \
| ./seppo.cgi abs2rel
https://example.com/foo/o/p-0/#1
o/p-0/#2
EOF
