
# #Seppo

## Ziel

Laien schreiben im Alleingang ins Internet.

Sicher, zuverlässig, verantwortungsvoll.

Wahlweise automatische Zweitveröffentlichung bei Twitter.com und im Fediverse.

https://mro.name/o/2021-09-30-181132-prototypefund11-222.txt

## Einsatz

Kommentare zu im Internet gefundenen URLs und andere kurze Texte.

Nachrichten anderer abonnieren. Bilder etc. veröffentlichen.

## Voraussetzungen

### /RC10/ Eine Domain reservieren

Wählen Sie einen Domainnamen und mieten Sie das kleinste Angebot bei einem Internetdienstleister, z.B. variomedia.de/hosting. Das enthält auch den notwendigen Ablageplatz.

Obwohl Sie technisch gesehen keine eigene Domain brauchen, ist dies der einzige Weg zur Eigenständigkeit. Auch über einen Anbieterwechsel hinaus.

Deshalb brauchen Sie doch eine.

Nehmen Sie einen Namen, der leicht zu sprechen und prägnant ist und aus Buchstaben, Ziffern und dem Bindestrich besteht. Vermeiden Sie Namen anderer oder fremde Marken.

### /RC20/ Apache Webserver

Gängiges Produkt zur Bereitstellung von Internetseiten durch Internetdienstleister.

Die #Seppo-Integration geschieht automatisch, es sind keine weiteren Vorbereitungen nötig.

### /RC30/ lighttpd Webserver

Alternatives Produkt zur Bereitstellung von Internetseiten mit Fans im Eigenbetrieb.

Ist im Vergleich mit Apache intern schlanker, aber erfordert anfangs Handarbeit.

Siehe lighttpd.conf

## Funktionen

### /RF10/ Installation / Löschung

1. laden Sie die Datei seppo.cgi herunter und kopieren Sie sie auf Ihren Webspace (Ihr Dienstleister hat sicher Anleitungen dafür)
2. besuchen Sie die entsprechende URL mit einem Browser: http://example.com/seppo.cgi
3. wählen Sie ein Passwort
4. Schreiben Sie los!

#Seppo besteht ausschließlich aus reinen Textdateien und Bildern auf Ihrem Webspace. Ohneˇ Speicherung woanders und ohne Datenbank. 

So bleibt #Seppo einfach zu

- inspizieren
- kopieren (Sicherungskopien)
- löschen

Zum Entfernen löschen Sie einfach seppo.cgi und alle davon erstellten Dateien in seiner Nachbarschaft vom Webspace.

### /RF20/ Öffentlicher Kanal für Ihre Besucher

Ihr #Seppo (http://example.com/) kann sowohl direkt mit einem Browser angesehen als auch
als Atom-Kanal abonniert werden.

### /RF30/ Neu/Bearbeiten/Löschen eines Eintrags

### /RF40/ Eintrag anheften

### /RF50/ Einen fremden Kanal abonnieren

Inkl. Benachrichtigung.

### /RF60/ Fediverse Antwort/Erwähnung

Inkl. Ändern/Löschen

### /RF70/ Twitter.com Antwort/Erwähnung

Inkl. Ändern/Löschen

### /RF80/ 4h-Taktung

Einholende Aktionen werden alle 4h eine Minute nach der vollen Stunde, sendende 5 Minuten vorher verarbeitet. So haben Sie den Streß der Neuigkeiten nur 3x pro Arbeitstag (Beginn, Mitte, Ende) und nicht ständig.

Für Dringendes haben Sie andere Kanäle.

## Daten

### /RD10/ Öffentlicher Kanal

Atom feed   urn:ietf:rfc:4287
Seitenweise urn:ietf:rfc:5005
Thematisch  urn:ietf:rfc:4685

### /RD20/ Schlagwort-Kanäle

### /RD30/ Sperren

Wiederholte Fehlversuche bei der Legitimation führen zu einer befristeten Sperre.

### /RD40/ POSSE

### /RD50/ Abonnements

## Sicherheit

Melden Sie Bedenken sofort an security@seppo.social.

Die wichtigsten Sicherheitsmerkmale von #Seppo sind der Ein-Benutzer-Ansatz, die Klartext Speicherung und die insgesamt geringe Komplexität. Alles ist im sicheren und qualitätsbewussten Ökosystem von OCaml geschrieben.

#Seppo ist ein CGI, was manche Leute als unsicher bezeichnen und Ihnen stattdessen möglicherweise zu PHP, Node oder ähnlichem raten. Deren jeweilige Basis-Software benötigt jedoch häufige Updates, bringt mehrere Dimensionen größere Komplexität (und damit Programmfehler) und sind zu guter letzt selbst CGIs.

#Seppo speichert alle Daten in Textdateien an Ort und Stelle (ohne Datenbank) und benötigt daher Schreibzugriff auf das Dateisystem des Webspace. Dies ist eine Angriffsfläche, mit der man vorsichtig sein muß. Aber es gibt nur genau einen Benutzer, der etwas verändern darf: Sie selbst. Also gibt es keine böswilligen Benutzer oder Angreifer von innen.

Es sehr wichtig, dass #Seppo ohne Legitimation nichts verändert - mit Ausnahme der Verwaltung von Sperren.

Die Legitimation braucht einen Schutz gegen Durchprobieren.

#Seppo schreibt niemals außerhalb seiner eigenen Ablage.

HTTPS wird empfohlen, ist aber nicht zwingend notwendig. Des weiteren folgt #Seppo den Empfehlungen von observatory.mozilla.org.

## Leistung

### /RP10/ Mengengerüst

Volumen ohne Leistungs-Einbußen:

- 100T Kurznachrichten
- 5T Schlagworte
- 25 Kurznachrichten pro Tag
- 1T Zeichen pro Kurznachricht
- 10T Abonnenten

### /RP20/ Kanal lesen

Eine einzelne HTTP-Anfrage (< 100KB) ist nur ein einfacher HTTP-GET an den Webserver.

### /RP30/ Kurznachricht Ändern

Anfügen, ein HTTP POST zu seppo.cgi < 1Sek. 

Verändern (gleiche Schlagworte), ein HTTP POST zu seppo.cgi < 1Sek.

Löschen, ein HTTP POST zu seppo.cgi < 3Sek.

## Qualität

| Qualität        | sehr gut  |  gut | normal | nicht relevant |
|-----------------|:---------:|:----:|:------:|:--------------:|
| Funktionalität  |           |      |    ×   |                |
| Zuverlässigkeit |           |  ×   |        |                |
| Benutzbarkeit   |     ×     |      |        |                |
| Effizienz       |           |      |    ×   |                |
| Änderbarkeit    |           |  ×   |        |                |
| Übertragbarkeit |           |      |    ×   |                |

## Glossar

- Atom-Kanal: =>Kanal
- Browser: Computerprogramm zum Abruf und Betrachtung von =>Webseiten
- CGI: Verfahren zur Zusammenarbeit von =>Webserver und weiteren Programme im Rechenzentrum. Seit 1993.
- Domain: Menschenlesbarer Name für eine Internetadresse z.B. eines =>Webservers, z.B. example.com. Wird zentral vergeben, kann man jahresweise mieten.
- Fediverse: Netzwerk aus zusammenarbeitenden, aber eigenständigen Anbietern, z.B. für Kurztexte.
- HTTP: Bestimmte Art der Kommunikation von Programmen untereinander, =>Browser und =>Webserver. Seit 1991.
- HTTPS: Geheimes =>HTTP.
- IndieWeb.org/POSSE: Zweitveröffentlichungsprinzip "erst zuhause, dann anderswo". Meist automatisch und mit Hinweis auf den Ursprung.
- Internetdienstleister: Anbieter, der z.B. =>Webspace, =>Domains oder Email-Konten betreibt und vermietet.
- Kanal: ähnlich einem Nachrichtenticker oder Telex. Ein maschinenlesbares Dokument im Format "Atom" https://de.wikipedia.org/wiki/Atom_(Format) (seit 2005) oder veraltet "RSS".
- OCaml: Programmiersprache und Werkzeuge aus dem französischen Forschungsinstitut INRIA. Seit 1996.
- Passwort: geheimer, individueller Erkennungstext. Sollte mindestens 12 Zeichen lang sein. Holprigkeit ist entgegen weiterverbreitetem Irrglauben nutzlos. Ein Passwort ist erst dann unsicher, wenn es öffentlich auf einer Müllhalde für verplapperte Passwörter von Facebook, Yahoo etc. herumliegt. Siehe https://glm.io/140363
- Twitter.com: großer Dienstanbieter zur Veröffentlichung von Kurztexten.
- URL: genaue Abruf-Adresse für Dokumente im Internet. Z.B. https://example.com/index.html. Beginnt in unserem Fall mit =>HTTPS, dann :// und dem =>Domain Namen.
- Webseite: ein Dokument im WWW mit Text, Bildern etc. lesbar mittels einem =>Browser.
- Webserver: Produkt zur Bereitstellung von =>Webseiten, in der Regel im Rechenzentrum beim Internetdienstleister.
- Webspace: Ablageplatz für =>Webseiten zur Bereitstellung durch einen =>Webserver.
- WWW: der Teil des Internets mit den =>Webseiten, der per =>HTTP abgerufen wird.
