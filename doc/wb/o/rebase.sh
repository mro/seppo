#!/bin/sh
cd "$(dirname "$0")" || exit 1

find . -name "index.xml" \
| xargs sed -I '' -e "s|https://dev.seppo.social/2024-03-11|http://localhost/wb|g"
