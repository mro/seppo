<?xml version="1.0" encoding="UTF-8"?>
<!--
      _  _   ____                         _  
    _| || |_/ ___|  ___ _ __  _ __   ___ | | 
   |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
   |_      _|___) |  __/ |_) | |_) | (_) |_| 
     |_||_| |____/ \___| .__/| .__/ \___/(_) 
                       |_|   |_|             

  Personal Social Web.

  Copyright (C) The #Seppo contributors. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.

  http://www.w3.org/TR/xslt/
-->
<xsl:stylesheet
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0">

  <!-- replace linefeeds with <br> tags -->
  <xsl:template name="linefeed2br">
    <xsl:param name="string" select="''"/>
    <xsl:param name="pattern" select="'&#10;'"/>
    <xsl:choose>
      <xsl:when test="contains($string, $pattern)">
        <xsl:value-of select="substring-before($string, $pattern)"/><br class="br"/><xsl:comment>Why do we see 2 br on Safari and output/@method=html here? http://purl.mro.name/safari-xslt-br-bug</xsl:comment>
        <xsl:call-template name="linefeed2br">
          <xsl:with-param name="string" select="substring-after($string, $pattern)"/>
          <xsl:with-param name="pattern" select="$pattern"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:output
    method="html"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>

  <xsl:variable name="xml_base">../</xsl:variable>
  <xsl:variable name="xml_base_pub" select="concat($xml_base,'o')"/>
  <xsl:variable name="skin_base" select="concat($xml_base,'themes/current')"/>
  <xsl:variable name="cgi_base" select="concat($xml_base,'seppo.cgi')"/>

  <xsl:template match="/">
    <xsl:apply-templates select="h:html"/>
  </xsl:template>

  <xsl:template match="h:html">
    <html xmlns="http://www.w3.org/1999/xhtml" bgcolor="darkgreen">
      <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <!-- https://developer.apple.com/library/IOS/documentation/AppleApplications/Reference/SafariWebContent/UsingtheViewport/UsingtheViewport.html#//apple_ref/doc/uid/TP40006509-SW26 -->
        <!-- http://maddesigns.de/meta-viewport-1817.html -->
        <!-- meta name="viewport" content="width=device-width"/ -->
        <!-- http://www.quirksmode.org/blog/archives/2013/10/initialscale1_m.html -->
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <!-- meta name="viewport" content="width=400"/ -->
        <link href="{$skin_base}/style.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" type="image/png" data-emoji="|S"/>
        <link rel="icon" type="image/jpg" href="../me-avatar.jpg"/>

        <title><xsl:value-of select="h:head/h:title"/></title>
      </head>
      <xsl:for-each select="h:body/h:form">
        <body onload="document.{@name}.title.focus();">
          <label form="{@name}" class="h1"><xsl:value-of select="../../h:head/h:title"/></label>
          <p><img
            width="600px" height="100px"
            alt="../app/ access permission check: ok (https://Seppo.mro.name/S1005)"
            title="if a security alert is visible here, ../app/ is publicly accessible from the www. See https://Seppo.mro.name/S1005"
            src="../app/i-must-be-403.svg"/></p>
          <p>You are invited, but not obliged, to have a look at the files on
          your webspace. You find a directory <code>themes/current/</code> that contains the
          overall look of your #Seppo!. Most importantly <a href="../themes/current/style.css"><code>style.css</code></a>.</p>
          
          <p>All files outside <code>./app/</code> are unpacked from
<code>seppo.cgi</code> or created during operation. To get a fresh copy,
delete a file and the marker file <code>delete-me-to-unpack-missing</code> and
you'll get a fresh copy after visiting <a href="../
seppo.cgi"><code>seppo.cgi</code></a>.</p>

          <h3>Banner</h3>
          <div id="banner">
            <img width="720" height="240" alt="Banner" src="../me-banner.jpg" title="adjust 'me-banner.jpg' next to seppo.cgi"/>
          </div>
          <p>Upload a jpg to your webspace and rename it to <code><a href="../me-banner.jpg">me-banner.jpg</a></code>
          (next to <code>seppo.cgi</code>). See also <a href="https://seppo.mro.name/S1007">S1007</a></p>

          <h3>Avatar</h3>
          <p id="avatar">
            <img alt="Avatar" src="../me-avatar.jpg" title="adjust 'me-avatar.jpg' next to seppo.cgi"/>
          </p>
          <p>Upload a jpg to your webspace and rename it to <code><a href="../me-avatar.jpg">me-avatar.jpg</a></code>
          (next to <code>seppo.cgi</code>). See also <a href="https://seppo.mro.name/S1007">S1007</a></p>

          <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
          </xsl:copy>
        </body>
      </xsl:for-each>
    </html>
  </xsl:template>

  <xsl:template match="h:input">
    <xsl:if test="@type != 'hidden'">
      <label for="{@name}" class="h3"><xsl:value-of select="@name"/></label>
    </xsl:if>
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="h:textarea">
    <label for="{@name}" class="h3"><xsl:value-of select="@name"/></label>
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
