
# Seppo

## Goal

Laypeople publish on the internet single-handedly.

Safe, reliable, responsibly.

Optionally cross-post to twitter and the fediverse. As required by IndieWeb.org/POSSE.

https://mro.name/o/2021-09-30-181132-prototypefund11-222_--_en.txt

## Use

Write small texts, e.g. comment on URLs found on the internet.

Enable subscription. Post images / enclosures.

## Preconditions

### /RC10/ Get a domain

Choose a domain name and buy the smallest package at any hoster e.g variomedia.de/hosting.

While you do not technically need a domain on your own, it is the only way to sovereignty.

Therefore in fact you need one.

### /RC20/ Apache webserver

Common for shared hosting. Setup is automatic, no other preparations necessary.

### /RC30/ lighttpd webserver

Common for self-hosting, has reduced complexity compared to Apache.

see lighttpd.conf

## Functions

### /RF10/ Install / Purge

1. copy the seppo.cgi file to your webspace
2. visit the according URL with your browser: http://example.com/seppo.cgi
3. choose a password
4. post!

Seppo consists strictly of plain text files and images on your webspace. There is
no hidden storage or database involved. So it remains simple to

- inspect
- copy (backup/restore)
- delete

To purge just delete seppo.cgi and all it's created files.

### /RF20/ Public feed for your Visitors

Your Seppo (http://example.com/) can be both viewed directly with a browser and
subscribed to as an atom feed.

### /RF30/ New/edit/delete public feed entry


### /RF40/ Subscribe/Follow public feed

incl. notification

### /RF50/ Fediverse reply/mention

incl. update/delete

### /RF60/ Twitter reply/mention

incl. update/delete

## Data

### /RD10/ Public feed

Atom feed urn:ietf:rfc:4287
Paged urn:ietf:rfc:5005
Threaded urn:ietf:rfc:4685

### /RD20/ Tag feed

### /RD30/ Bans

### /RD40/ Subscriptions

## Security

Report any concerns to security@seppo.social.

The main security features of Seppo are the single-user approach, its plain text
storage and the overall low complexity. All written in the safe and quality-aware
ecosystem OCaml.

Seppo is built as a CGI, which some people decry as insecure and may advise you to
use PHP, Node or the like instead. However, their respective engines need frequent updates, bring several
orders of magnitude more complexity (and bugs) and are CGIs themselves.

Seppo stores all data in mere text files in place (no database involved) and
therefore needs write access to the filesystem of its webspace. This is an attack
vector to be careful about. There is exactly one user allowed to write: you. So
there are no malicious users or attackers from inside.

It is paramount that Seppo doesn't modify anything unauthenticated - only exception
being ban management.

Authentication has brute-force protection.

Seppo never writes outside it's location.

HTTPS is encouraged while not mandatory. Otherwise Seppo follows best practices at
observatory.mozilla.org.

## Performance

### /RP10/ Quantity structure

numbers before the UX degrades:

- 100k posts
- 5k tags
- 25 posts per day
- 1k characters per post
- 10k subscribers

### /RP20/ Read Feed

A single HTTP request < 100k just a plain HTTP GET to the webserver.

### /RP30/ Modify Entry

Adding a new entry, a HTTP POST to the seppo.cgi < 1sec. 

Modifying an entry (same tags), a HTTP POST to the seppo.cgi < 1sec. 

Delete an entry, a HTTP POST to the seppo.cgi < 3sec.

## Quality

| Quality         | very good | good | normal | irrelevant |
|-----------------|:---------:|:----:|:------:|:----------:|
| Functionality   |           |      |    ×   |            |
| Reliability     |           |  ×   |        |            |
| Usability       |     ×     |      |        |            |
| Efficiency      |           |      |    ×   |            |
| Changeability   |           |  ×   |        |            |
| Portability     |           |      |    ×   |            |

