(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

type t = T of string

let fallback = Timedesc.Time_zone.(local () |> Option.value ~default:utc)

let epoch = T "1970-01-01 00:00:00Z"

let compare a b =
  let T a = a
  and T b = b in
  a |> String.compare b

let tz_offset_s tz t0 =
  match t0
        |> Timedesc.Utils.timestamp_of_ptime
        |> Timedesc.of_timestamp ~tz_of_date_time:tz with
  | None   -> 0
  | Some x ->
    (match x |> Timedesc.offset_from_utc with
     | `Single t
     | `Ambiguous (t,_) -> t)
    |> Timedesc.Timestamp.to_float_s
    |> Int.of_float

let to_string ?(tz = fallback) t0 =
  let tz_offset_s = t0 |> tz_offset_s tz in
  t0 |> Ptime.to_rfc3339 ~tz_offset_s

let of_ptime ?(tz = fallback) t0 =
  T (t0 |> to_string ~tz)

let to_ptime (T s) =
  match s |> Ptime.of_rfc3339 with
  | Error _    -> Error "expected rfc3339"
  | Ok (t,_,_) -> Ok t

let to_xml (n : string) (T s) =
  `El (((Xml.ns_a,n),[]),[`Data s])
