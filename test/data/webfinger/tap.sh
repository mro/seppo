#!/bin/sh
cd "$(dirname "$0")" || exit 1

[ "$1" = "" ] && {
cat <<EOF
give me webfinger addresses in the form @demo@seppo.social
EOF
exit 1
}

while [ "$1" != "" ] ; do
  [ -r "$1".json ] || {
    uid="$(echo "$1" | cut -f 2 -d @)"
    hos="$(echo "$1" | cut -f 3 -d @)"
    url="https://$hos/.well-known/webfinger?resource=acct:$uid@$hos"
    curl --output "$1".head --silent --location --head "$url"
    curl --output "$1".json --silent --location "$url"
  }
  ls -l "$1".*
  shift
done
