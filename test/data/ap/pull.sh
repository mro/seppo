#!/bin/sh
cd "$(dirname "$0")" || exit 1

while read -r h n pro
do
  echo "$h -> $n -> $pro"
  lo="$(echo "$h" | cut -d '@' -f2)"
  ho="$(echo "$h" | cut -d '@' -f3)"
  curl \
    --create-dirs \
    --dump-header "webfinger/$h.head" \
    --header "Accept: application/jrd+json" \
    --location \
    --max-time 5.0 \
    --output "webfinger/$h.json" \
    --silent \
     "$ho/.well-known/webfinger?resource=acct:$lo@$ho" \
   > /dev/null
  curl \
    --create-dirs \
    --dump-header "profile/$h.head" \
    --header 'Accept: application/ld+json; profile="https://www.w3.org/ns/activitystreams"' \
    --header 'Accept: application/activity+json' \
    --location \
    --max-time 5.0 \
    --output "profile/$h.json" \
    --silent \
     "$pro" \
   > /dev/null
done \
< accounts.txt
