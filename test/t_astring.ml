
open Alcotest
open Astring

let tc_affix = "affix", `Quick, (fun () ->
    String.is_prefix ~affix:"Hell" "Hello, world!"
    |> check bool __LOC__ true
  )

let tc_sub = "sub", `Quick, (fun () ->
    String.Sub.v ~start:4 "Hello, world!"
    |> String.Sub.to_string
    |> check string __LOC__ "o, world!"
  )

let tc_cuts = "cuts", `Quick, (fun () ->
    let sep = "|" in
    (match "a|b|c|" |> String.cuts ~sep with
       ["a"; "b"; "c"; ""] -> ()
     | _ -> fail __LOC__) ;
    let is_sep c = c = '|' in
    match "a|b|c|" |> String.fields ~is_sep with
      ["a"; "b"; "c"; ""] -> ()
    | _ -> fail __LOC__
  )

let tst : return test = __FILE__, [
    tc_affix ;
    tc_sub ;
    tc_cuts ;
  ]

let () =
  run
    "seppo_suite" [
    tst ;
  ]
