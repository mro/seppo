open Alcotest

let set_up () =
  Unix.chdir "../../../test/"

let tc_version () =
  Sqlite3.sqlite_version () / 1_000_000
  |> check int __LOC__ 3

let tc_open () =
  let db = ":memory:" |> Sqlite3.db_open ~memory:true in
  db
  |> Sqlite3.db_close
  |> check bool __LOC__ true

let () =
  run
    "seppo_suite" [
    __FILE__ , [
      "setup",                             `Quick, set_up;
      "tc_version",                        `Quick, tc_version;
      "tc_open",                           `Quick, tc_open;
    ]
  ]

