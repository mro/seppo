open Alcotest
open Seppo_lib

let set_up () =
  Mirage_crypto_rng_unix.use_default ();
  Unix.chdir "../../../test/"

let tc_script_url_dir () =
  "/seppo.cgi" |> Cgi.Request.script_url_dir |> check string __LOC__ "/";
  "/apchk.cgi" |> Cgi.Request.script_url_dir |> check string __LOC__ "/";
  "/cgi-bin/subdi/seppo.cgi" |> Cgi.Request.script_url_dir |> check string __LOC__ "/subdi/";
  "/suhu/.cgi" |> Cgi.Request.script_url_dir |> check string __LOC__ "/suhu/";
  "/cgi-bin/suha/.cgi" |> Cgi.Request.script_url_dir |> check string __LOC__ "/suha/";
  "/cgi-bun/suha/.cgi" |> Cgi.Request.script_url_dir |> check string __LOC__ "/cgi-bun/suha/";
  ()

let tc_from_env () =
  let env l s = l |> List.assoc_opt s in
  let r = Cgi.Request.(from_env ~getenv_opt:(env [
      (* hCONTENT_LENGTH,  "CONTENT_LENGTH" ; *)
      (* hCONTENT_TYPE,    "CONTENT_TYPE" ; *)
      (* hHTTP_COOKIE,     "HTTP_COOKIE" ; *)
      (* hHTTP_HOST,       "example.com" ; *)
      (* hHTTP_USER_AGENT, "agent" ; *)
      (* hHTTP_X_FORWARDED_FOR, "217.88.75.197" ; *)
      (* hHTTP_X_FORWARDED_PROTO, "https" ; *)
      (* hHTTPS,           "HTTPS" ; *)
      (* hPATH_INFO,       "path_info" ; *)
      (* hQUERY_STRING,    "" ; *)
      hREMOTE_ADDR,     "192.0.2.7" ; (* https://www.rfc-editor.org/rfc/rfc5735#section-3 *)
      hREQUEST_METHOD,  "GET" ;
      (* hREQUEST_URI,     "/i.cgi" ; *)
      hSCRIPT_NAME,     "/i.cgi" ;
      hSERVER_NAME,     "example.com" ;
      hSERVER_PORT,     "80" ;
    ]) () |> consolidate |> proxy) |> Result.get_ok in
  r.host        |> check string __LOC__ "example.com";
  r.path_info   |> check string __LOC__ "";
  r.remote_addr |> check string __LOC__ "192.0.2.7";
  r.scheme      |> check string __LOC__ "http";
  r.script_name |> check string __LOC__ "/i.cgi";
  r.server_port |> check string __LOC__ "80";
  ()

let tc_from_env_proxy () =
  let env l s = l |> List.assoc_opt s in
  let r = Cgi.Request.(from_env ~getenv_opt:(env [
      (* hCONTENT_LENGTH,  "CONTENT_LENGTH" ; *)
      (* hCONTENT_TYPE,    "CONTENT_TYPE" ; *)
      (* hHTTP_COOKIE,     "HTTP_COOKIE" ; *)
      (* hHTTP_HOST,       "example.com" ; *)
      (* hHTTP_USER_AGENT, "agent" ; *)
      hHTTP_X_FORWARDED_FOR, "192.0.2.7" ; (* https://www.rfc-editor.org/rfc/rfc5735#section-3 *)
      hHTTP_X_FORWARDED_PROTO, "https" ;
      (* hHTTPS,           "HTTPS" ; *)
      (* hPATH_INFO,       "path_info" ; *)
      (* hQUERY_STRING,    "" ; *)
      hREMOTE_ADDR,     "127.0.0.1" ;
      hREQUEST_METHOD,  "GET" ;
      (* hREQUEST_URI,     "/i.cgi" ; *)
      hSCRIPT_NAME,     "/i.cgi" ;
      hSERVER_NAME,     "example.com" ;
      hSERVER_PORT,     "80" ;
    ]) () |> consolidate |> proxy) |> Result.get_ok in
  r.host        |> check string __LOC__ "example.com";
  r.path_info   |> check string __LOC__ "";
  r.remote_addr |> check string __LOC__ "192.0.2.7";
  r.scheme      |> check string __LOC__ "https";
  r.script_name |> check string __LOC__ "/i.cgi";
  r.server_port |> check string __LOC__ "443";
  ()

let tc_abs () =
  let env l s = l |> List.assoc_opt s in
  let r = Cgi.Request.(from_env ~getenv_opt:(env [
      hHTTP_X_FORWARDED_FOR, "192.0.2.7" ; (* https://www.rfc-editor.org/rfc/rfc5735#section-3 *)
      hHTTP_X_FORWARDED_PROTO, "https" ;
      hREMOTE_ADDR,     "127.0.0.1" ;
      hREQUEST_METHOD,  "GET" ;
      hSCRIPT_NAME,     "/i.cgi" ;
      hSERVER_NAME,     "example.com" ;
      hSERVER_PORT,     "80" ;
    ]) () |> proxy) |> Result.get_ok in
  r |> Cgi.Request.srvr
  |> Uri.to_string |> check string __LOC__ "https://example.com";
  r |> Cgi.Request.abs
  |> Uri.to_string |> check string __LOC__ "https://example.com/i.cgi"

let () =
  run
    "seppo_suite" [
    __FILE__ , [
      "set_up"            , `Quick, set_up;
      "tc_script_url_dir ", `Quick, tc_script_url_dir ;
      "tc_from_env "      , `Quick, tc_from_env ;
      "tc_from_env_proxy ", `Quick, tc_from_env_proxy ;
      "tc_abs "           , `Quick, tc_abs ;
    ]
  ]
