(entry
  (title "🐫 📺 25 Years of #OCaml: Xavier #@Leroy - Watch OCaml")
  (id ag8gdh2)
  (updated "2022-01-18T08:41:30+01:00")
  (published "2022-01-18T08:37:12+01:00")
  (link "https://watch.ocaml.org/videos/watch/e1ee0fc0-50ef-4a1c-894a-17df181424cb")
  (categories @Leroy AD2021 OCaml PeerTube 🐫 📺)
  (content "\"Professor Xavier Leroy -- the primary original author and leader of the OCaml project -- reflects on 25 years of the OCaml language at his OCaml Workshop #AD2021 keynote speech.\" #PeerTube

via https://discuss.ocaml.org/t/ann-first-announcement-of-bechamel/9164/2?u=mro")
)
