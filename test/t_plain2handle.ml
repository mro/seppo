(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib

let test_plain2handle () =
  let l = "a\n(@alice@example.com) \nc https://Seppo.Social/d/?e#f followed by @bob@example.com."
          |> Lexing.from_string
          |> Plain2handle.handle [] in
  match l with
  | [a;b] ->
    a |> Assrt.equals_string __LOC__ "bob@example.com";
    b |> Assrt.equals_string __LOC__ "alice@example.com"
  | l -> l |> List.length |> Assrt.equals_int __LOC__ 200000

let () =
  Unix.chdir "../../../test/";
  test_plain2handle ();
  assert true

