(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib
open Rfc7565

let test_string' () =
  let u = "acct:alice@example.com" |> of_string |> Result.get_ok in
  let T u' = u in
  u' |> Uri.scheme |> Option.get |> Assrt.equals_string __LOC__ scheme;
  u' |> Uri.userinfo |> Option.get |> Assrt.equals_string __LOC__ "alice";
  u' |> Uri.host |> Option.get |> Assrt.equals_string __LOC__ "example.com";
  u |> to_string |> Assrt.equals_string __LOC__ "acct:alice@example.com";

  make ~local:"alice" ~domain:"example.com" ()
  |> to_string ~prefix:"@"
  |> Assrt.equals_string __LOC__ "@alice@example.com";

  Format.asprintf "%a" Rfc7565.pp_hum u
  |> Assrt.equals_string __LOC__ "@alice@example.com";

  assert true

let () =
  Unix.chdir "../../../test/";
  test_string' ();
  assert true
