let equals_int = Assert.assert_equals_int
let equals_float name eps exp fou = Assert.assert_equals_float name exp fou eps
let equals_string = Assert.assert_equals_string

let equals_uri l e f =
  let e = e |> Uri.to_string
  and f = f |> Uri.to_string in
  equals_string l e f

let equals_int32 l e f =
  let e = e |> Int32.to_string
  and f = f |> Int32.to_string in
  equals_string l e f

let equals_optint l e f =
  let e = e |> Optint.to_string
  and f = f |> Optint.to_string in
  equals_string l e f

let equals_none l v =
  (match v with
   | None -> "None"
   | Some _ -> "Some ..."
  ) |> equals_string l "None"
