
open Seppo_lib

(* https://erratique.ch/software/xmlm/doc/Xmlm/index.html#outns *)
let test_ezxmlm_out () =
  Logr.info (fun m -> m "%s.%s" "xmlm_test" "text_ezxmlm_out");
  []
  |> Ezxmlm.to_string
  |> Assrt.equals_string __LOC__ "";
  [ `El ((("", "uhu"), []), []) ]
  |> Ezxmlm.to_string
  |> Assrt.equals_string __LOC__ "<uhu/>";
  [ `El ((("", "e"), [ (("", "a"), "v") ]), [ `Data "foo" ]) ]
  |> Ezxmlm.to_string
  |> Assrt.equals_string __LOC__ "<e a=\"v\">foo</e>";
  [ `El ((("", "e"), [ (("", "xmlns"), "A") ]), [ `Data "foo" ]) ]
  |> Ezxmlm.to_string
  |> Assrt.equals_string __LOC__ "<e xmlns=\"A\">foo</e>";
  [ `El ((("A", "e"), [((Xmlm.ns_xmlns, "xmlns"), "A")]), [`Data "foo"]) ]
  |> Ezxmlm.to_string
  |> Assrt.equals_string __LOC__ "<e xmlns=\"A\">foo</e>";
  [ `El ((("N", "e"), [((Xmlm.ns_xmlns, "n"), "N")]), [`Data "foo"]) ]
  |> Ezxmlm.to_string
  |> Assrt.equals_string __LOC__ "<n:e xmlns:n=\"N\">foo</n:e>";
  assert true

let test_ezxmlm_out2() =
  Logr.info (fun m -> m "%s.%s" "xmlm_test" "text_ezxmlm_out2");
  [`El ((("","elm"),[(("","nam"),"val")]),[])]
  |> Ezxmlm.to_string
  |> Assrt.equals_string __LOC__ {|<elm nam="val"/>|};
  [`El ((("uri 2","root"),[
       ((Xmlm.ns_xmlns,"n2"),"uri 2");
     ]),[
          `El ((("uri 2","c1"),[
            ]),[
                 `Data "uh";
                 `Data "u";
               ])]);
  ]
  |> Ezxmlm.to_string ~decl:true
  |> Assrt.equals_string __LOC__ {|<?xml version="1.0" encoding="UTF-8"?>
<n2:root xmlns:n2="uri 2"><n2:c1>uhu</n2:c1></n2:root>|};
  assert true

let test_xmlm_signals () =
  Logr.info (fun m -> m "%s.%s" "xmlm_test" "test_xmlm_signals");
  let dst = Buffer.create 1024 in
  Buffer.add_string dst {|<?xml version="1.0" encoding="UTF-8"?>
|};
  Buffer.add_string dst "<!-- huhu -->\n";
  let o = Xmlm.make_output ~decl:false ~indent:(Some 2) (`Buffer dst) in
  let out = Xmlm.output o in
  out (`Dtd None);
  out (`El_start (("uri","e"),[
      ((Xmlm.ns_xmlns,"n"),"uri");
      ("","k"),"v"]));
  out (`Data "uhu");
  out `El_end;
  dst
  |> Buffer.to_bytes
  |> Bytes.to_string
  |> Assrt.equals_string __LOC__ {|<?xml version="1.0" encoding="UTF-8"?>
<!-- huhu -->
<n:e xmlns:n="uri" k="v">
  uhu
</n:e>|};
  assert true

let test_xmlm_frag () =
  Logr.info (fun m -> m "%s.%s" "xmlm_test" "test_xmlm_signals");
  let x : _ Xmlm.frag = `El ((("uri", "e"), [
      ((Xmlm.ns_xmlns, "n"), "uri");
      (("", "k"), "v");
    ]), [`Data "uhu"]) in
  let dst = Buffer.create 1024 in
  Buffer.add_string dst {|<?xml version="1.0" encoding="UTF-8"?>
|};
  Buffer.add_string dst "<!-- huhu -->\n";
  let o = Xmlm.make_output ~decl:false ~indent:(Some 2) (`Buffer dst) in
  Xmlm.output_doc_tree (fun x -> x) o (None,x);
  dst
  |> Buffer.to_bytes
  |> Bytes.to_string
  |> Assrt.equals_string __LOC__ {|<?xml version="1.0" encoding="UTF-8"?>
<!-- huhu -->
<n:e xmlns:n="uri" k="v">
  uhu
</n:e>|};
  assert true

let () =
  Unix.chdir "../../../test/";
  test_ezxmlm_out ();
  test_ezxmlm_out2 ();
  test_xmlm_signals ();
  test_xmlm_frag ();
  assert true
