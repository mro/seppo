(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let tail x =
  Assrt.equals_string __LOC__ "ok" (if x |> Result.is_ok then "ok" else "no")

open Seppo_lib
open Alcotest

let set_up = "setup", `Quick, (fun () ->
    Mirage_crypto_rng_unix.use_default ();
    Unix.chdir "../../../test/"
  )

let test_sift_handles () =
  let h = {|Some text with @
linebreaks and @sepp0@example.com. And
@other@example.com, even a @third@example.com!|} |> Main.find_handles in
  match h with
  | [a;b;c] ->
    a |> Assrt.equals_string __LOC__ "third@example.com";
    b |> Assrt.equals_string __LOC__ "other@example.com";
    c |> Assrt.equals_string __LOC__ "sepp0@example.com"
  | l -> l |> List.length |> Assrt.equals_int __LOC__ 30000

let fn = Mcdb.Cdb "tmp/main.cdb"
let Mcdb.Cdb fn' = fn

let test_sift_tags () =
  Logr.debug (fun m -> m "%s.%s" "Main_test" "test_sift_tags");
  let open Rfc4287 in
  Unix.(try unlink fn' with Unix_error (ENOENT, "unlink", _) -> ());
  File.touch fn';
  let published = Rfc3339.T "2023-03-09T12:34:56+01:00" in
  let author = {Rfc4287.Person.empty with name = "uhu"} in
  let lang = Rfc4646 "en" in
  let uri = Uri.empty in
  let title = {|Just a #note|} in
  let content = {|th some #more #Hashtags.|} in
  (let* e = Entry.from_text_plain ~published ~author ~lang ~uri title content in
   let* e : Entry.t = Main.sift_tags fn e in
   let li = e.categories in
   li |> List.length |> Assrt.equals_int __LOC__ 3;
   let (Category.Label (Single la),Category.Term (Single te),sc) = li |> List.hd in
   la |> Assrt.equals_string __LOC__ "note";
   te |> Assrt.equals_string __LOC__ "note";
   sc |> Uri.to_string |> Assrt.equals_string __LOC__ "o/t/";
   let li = li |> List.tl in
   let (Category.Label (Single la),Category.Term (Single te),sc) = li |> List.hd in
   la |> Assrt.equals_string __LOC__ "more";
   te |> Assrt.equals_string __LOC__ "more";
   sc |> Uri.to_string |> Assrt.equals_string __LOC__ "o/t/";
   let li = li |> List.tl in
   let (Category.Label (Single la),Category.Term (Single te),sc) = li |> List.hd in
   la |> Assrt.equals_string __LOC__ "Hashtags";
   te |> Assrt.equals_string __LOC__ "Hashtags";
   sc |> Uri.to_string |> Assrt.equals_string __LOC__ "o/t/";
   let li = li |> List.tl in
   li |> List.length |> Assrt.equals_int __LOC__ 0;
   Ok e) |> tail;
  assert true

let test_tmap_from_cdb () =
  let tm = Tag.Tmap.empty in
  let tm = Tag.Tmap.add "a" "A" tm in
  Tag.Tmap.find "a" tm |> Assrt.equals_string __LOC__ "A";
  assert true

module Apjob = struct
  module Notify = struct
    let encode = "encode", `Quick, (fun () ->
        Main.Apjob.Notify.encode
          ("https://example.com/sender/message#3462" |> Uri.of_string)
          ("https://example.com/recipient/inbox" |> Uri.of_string, "unused" |> Uri.of_string)
          (Ezjsonm.int 17495)
        |> Csexp.to_string
        |> check string __LOC__ {|(1:239:https://example.com/sender/message#34626:notify(35:https://example.com/recipient/inbox6:unused5:17495))|}
      )
  end
end

let () =
  run
    "seppo_suite" [
    __FILE__ , [
      set_up;
      Apjob.Notify.encode;
    ]
  ];
  test_sift_handles ();
  test_sift_tags ();
  test_tmap_from_cdb ();
  assert true
