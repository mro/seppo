#!/bin/sh

ls \
./res/static/app/var/lib/me-banner.jpg \
./res/static/app/var/lib/me-avatar.jpg \
./res/static/logo.png \
./doc/dirs/o/m-0/x359u5k.png \
./doc/dirs/o/m/aad78zt.jpg \
./doc/dirs/app/var/lib/e-0/x359u5k.png \
./doc/dirs/app/var/lib/e-2342/aad78zt.jpg \
| while read -r fi
do
    echo $fi
    convert $fi \
      -set schema.DC "http://purl.org/dc/elements/1.1/" \
      -set schema.DCTERMS "http://purl.org/dc/terms/" \
      -set DCTERMS.rightsHolder "The #Seppo contributors https://Seppo.Social/sourcecode" \
      -set DC.license "https://www.gnu.org/licenses/gpl-3.0" \
      -set DC.rights "GPL-3.0-only" \
      -set DC.source "https://Seppo.Social/sourcecode" \
      -set Comment "https://Seppo.Social" \
    $fi
done
